﻿const express = require('express');
const router = express.Router();
const userService = require('./user.service');

// routes
router.post('/authenticate', authenticate);
router.get('/', getAll);
router.post('/', addPost)
router.delete('/:id', deletePost)

module.exports = router;

function authenticate(req, res, next) {
    userService.authenticate(req.body)
        .then(user => user ? res.json(user) : res.status(400).json({ message: 'Username or password is incorrect' }))
        .catch(err => next(err));
}

function getAll(req, res, next) {
    userService.getAll()
        .then(post => res.json(post))
        .catch(err => next(err));
}

function addPost(req, res, next) {
    const todoPost = {
        title: req.body.title
    }

    userService.addPost(req.body)
        .then(post => res.status(201).send({
            message: 'todo added',
            success: 'true',
            todoPost
        }))
        .catch(err => next(err))
}

function deletePost(req, res, next) {
    userService.deletePost(req.params.id)
        .then(res.status(200).send({
            message: 'post is deleted',
        }))
        .catch(err => next(err))

    return res.status(404).send({
        message: 'post not found'
    })
}