﻿const config = require('config.json');
const jwt = require('jsonwebtoken');

// users hardcoded for simplicity, store in a db for production applications
//import db from '../db/db';

const users = [{ id: 1, username: 'test', password: 'test', firstName: 'Test', lastName: 'User' }];
const todos =  [
    {
      id: 1,
      title: "lunch",
      description: "Go for lunc by 2pm"
    },
    {
        id: 2,
        title: "shop",
        description: "Buy sneakers"
      }
];

let id = 3;

module.exports = {
    authenticate,
    getAll,
    addPost,
    deletePost
};

async function authenticate({ username, password }) {
    const user = users.find(u => u.username === username && u.password === password);
    if (user) {
        const token = jwt.sign({ sub: user.id }, config.secret);
        const { password, ...userWithoutPassword } = user;
        return {
            ...userWithoutPassword,
            token
        };
    }
}

async function getAll() {
    return todos;
}

async function addPost({ title }) {

    // const todoPost = {
    //     id: id,
    //     title: title
    // }

    todos.push({"id": id, "title": title})
    id++
}


async function deletePost(_id) {
    const id = parseInt(_id , 10)

    todos.map((post, index) => {
        if (post.id == id) {
        todos.splice(index, 1);
        }
    })
}
