﻿require('rootpath')();
const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');
const jwt = require('_helpers/jwt');
const errorHandler = require('_helpers/error-handler');

const csp = require('express-csp-header');

app.use(csp({
    policies: {
        'default-src': [csp.SELF],
        // 'script-src': [csp.SELF, csp.INLINE, 'localhost'],
        'style-src': [csp.SELF, 'localhost'],
        'img-src': ['data:', 'localhost'],
        'worker-src': [csp.NONE],
        'block-all-mixed-content': true
    }
}));

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());

// use JWT auth to secure the api
app.use(jwt());

// api routes
app.use('/api/v1', require('./users/users.controller'));

// global error handler
app.use(errorHandler);

// start server
const port = process.env.NODE_ENV === 'production' ? 80 : 4000;
const server = app.listen(port, function () {
    console.log('Server listening on port ' + port);
});
