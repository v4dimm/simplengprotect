﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { User } from '@/_models';
import { Post } from '@/_models/post';

@Injectable({ providedIn: 'root' })
export class UserService {
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<Post[]>(`${config.apiUrl}/api/v1`);
    }

    register(user: User) {
        return this.http.post(`${config.apiUrl}/api/v1/register`, user);
    }

    delete(id: number) {
        return this.http.delete(`${config.apiUrl}/api/v1/${id}`);
    }

    addTodo(newTodo: Post) {
        return this.http.post(`${config.apiUrl}/api/v1`, newTodo);
    }
}