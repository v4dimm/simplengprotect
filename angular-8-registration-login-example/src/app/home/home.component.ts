﻿import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';

import { DomSanitizer, SafeHtml, SafeValue, Title} from '@angular/platform-browser'

import { User } from '@/_models';
import { Post } from '../_models/post';
import { UserService, AuthenticationService } from '@/_services';
import { HttpClient } from '@angular/common/http';
import { from } from 'rxjs';

@Component({ templateUrl: 'home.component.html' })
export class HomeComponent implements OnInit {
    currentUser: User;
    posts: Post[] = [];

    todoTitle = '';
    
    source: SafeHtml

    constructor(
        private authenticationService: AuthenticationService,
        private userService: UserService,
        private http: HttpClient,

        private sanitizer: DomSanitizer

    ) {
        this.currentUser = this.authenticationService.currentUserValue;
        this.source = this.sanitizer.bypassSecurityTrustHtml('<h1>Can you hack me?</h1>')
    }
    

    ngOnInit() {
        this.loadAllPosts();
    }

    private loadAllPosts() {
        this.userService.getAll()
        .subscribe(post => {
            console.log('Response: ', post)
            this.posts = post
        })
//            .pipe(first())
//            .subscribe(posts => this.posts = posts);
    }

    addTodo() {
        if (!this.todoTitle.trim()) {
            return
        }

        const newTodo: Post = {
            title: this.todoTitle,
        }

        newTodo.description = newTodo.title

        // 1 - just use any html
//        this.source = this.sanitizer.bypassSecurityTrustHtml(newTodo.description)        

        // 2 - sanitize any untrusted code (see: https://angular.io/api/core/SecurityContext)
        this.source = this.sanitizer.sanitize(3, newTodo.title)

        // 3 - paste html with default secure
//        this.source = newTodo.title


        this.http.post<Post>('http://localhost:4000/api/v1/', newTodo)
            .subscribe(todo => {
                console.log(newTodo)

                this.posts.push(todo)

                this.todoTitle = ''
        })
        this.loadAllPosts();
    }
    

    deletePost(id: number) {
        this.http.delete<void>(`http://localhost:4000/api/v1/${id}`)
            .subscribe(() => {
                this.posts = this.posts.filter( t => t.id !== id)
            })
    }

}